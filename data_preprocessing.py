#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import pdb
from functions import *

df = segment_readings.copy()
df = df.rename(columns={"imei": "IMEI"})

df = df[df["IMEI"].isin(imei_list)]
print len(df)
df.trip_tstamp = pd.to_datetime(df.trip_tstamp, errors='coerce')
df["datetime"] = df.trip_tstamp + pd.Timedelta('05:30:00')
df['date'] = df['datetime'].dt.date.astype(str)
df['time'] = df['datetime'].dt.time.astype(str)
df['distance'] = (df['speed']/3.6) * 2.4 / 1000
df["Speed Band Interval Five"] = map(speed_band_interval_five, df["speed"])
df["Speed Band Interval Ten"] = map(speed_band_interval_ten, df["speed"])

df["One Hour Time Band"] = map(one_hour_time_band, df["time"])
df["Three Hour Time Band"] = map(three_hour_time_band, df["time"])

df["Over-speeding"] = map(over_speed, df["speed"])
df["Frontal Gap (ft)"] = map(tail_dist, df["speed"])
df["Frontal Gap (ft)"] = df["Frontal Gap (ft)"].round(0)
df['speed'] = df['speed'].round(0)

df["Coordinates"] = df["latitude"].round(5).astype(str) + "," + df["longitude"].round(5).astype(str)
df = df.merge(mappings, on=["IMEI"], how="outer")

df = df[['Organization ID', 'IMEI', 'Driver', 'Vehicle', 'Contact', 'Licence', 'Vehicle Type', 'datetime', 'date',
         'time', 'latitude', 'longitude', 'Coordinates', 'speed', 'distance', 'Frontal Gap (ft)', 'One Hour Time Band',
         'Three Hour Time Band', 'Speed Band Interval Five', 'Speed Band Interval Ten', 'tailgating_band',
         'speed_lane_change_band', 'speed_rolling_band', 'speed_pitch_angle_band', 'Over-speeding',
         'speed_braking_band', 'tailgating_speed_braking_band', 'speed_orientation_band']]

df = df.dropna(axis=0, subset=['speed'])

list_of_imei = df["IMEI"].unique().tolist()
final_data = pd.DataFrame()
print "Initial Count: ", len(final_data)

for i in range(len(list_of_imei)):
    data = df.copy()
    data = data[data["IMEI"] == list_of_imei[i]]
    print "IMEI: ", list_of_imei[i]
    print "IMEI Data Count: ", len(data)
    data.sort_values(by=['datetime'], inplace=True)
    data = data.reset_index(drop=True)
    data["datetime_next"] = data["datetime"].shift(periods=-1)
    data["datetime"] = pd.to_datetime(data["datetime"])
    data["datetime_next"] = pd.to_datetime(data["datetime_next"])
    data["delta time (sec)"] = data["datetime_next"] - data["datetime"]
    data["delta time (sec)"] = data["delta time (sec)"].dt.total_seconds()
    data["speed_next"] = data["speed"].shift(periods=-1)
    data["delta speed (kmph)"] = data["speed_next"] - data["speed"]
    data = data[data["delta time (sec)"] != 0]
    data["dec"] = map(braking, data["delta time (sec)"], data["delta speed (kmph)"])

    data['emergency_braking_band'] = data.apply(add_band, axis=1)
    data["emergency_braking_band"] = data["emergency_braking_band"].fillna(999.0)
    data['emergency_braking_band'] = data['emergency_braking_band'].astype("Float64").astype('Int64')
    print "IMEI Count before appending: ", len(data)

    data = data[['Organization ID', 'IMEI', 'Driver', 'Vehicle', 'Contact', 'Licence', 'Vehicle Type', 'datetime',
                 'date', 'time', 'latitude', 'longitude', 'Coordinates', 'speed', 'distance', 'Frontal Gap (ft)',
                 'One Hour Time Band', 'Three Hour Time Band', 'Speed Band Interval Five', 'Speed Band Interval Ten',
                 'tailgating_band', 'speed_lane_change_band', 'speed_rolling_band', 'speed_pitch_angle_band',
                 'Over-speeding', 'emergency_braking_band', 'tailgating_speed_braking_band', 'speed_orientation_band']]

    final_data = final_data.append(data)
    print "Current Count: ", len(final_data)

print "Final Count: ", len(final_data)


result = final_data.copy()
result = result.merge(new_collision_cals, on=["Vehicle Type", "Speed Band Interval Five", "emergency_braking_band",
                                              "tailgating_band"], how="outer")
result["collision_risk"] = result["collision_risk"].fillna(999.0)
result['collision_risk'] = result['collision_risk'].astype("Float64").astype('Int64')
result = result.dropna(axis=0, subset=['IMEI'])

result = result[['Organization ID', 'IMEI', 'Driver', 'Vehicle', 'Contact', 'Licence', 'Vehicle Type', 'datetime',
                 'date', 'time', 'latitude', 'longitude', 'Coordinates', 'speed', 'distance', 'Frontal Gap (ft)',
                 'One Hour Time Band', 'Three Hour Time Band', 'Speed Band Interval Five', 'Speed Band Interval Ten',
                 'tailgating_band', 'speed_lane_change_band', 'speed_rolling_band', 'speed_pitch_angle_band',
                 'Over-speeding', 'emergency_braking_band', 'collision_risk', 'speed_orientation_band']]


print result.head(10)
result.to_csv("./report_card_automation_test_new_braking_collision_risk.csv", index=False)
print "File Saved"
pdb.set_trace()
#
# result["Over-speeding"] = map(over_speed, result["speed"])
# result["Frontal Gap (ft)"] = map(tail_dist, result["speed"])
# result["Frontal Gap (ft)"] = result["Frontal Gap (ft)"].round(0)
# result['speed'] = result['speed'].round(0)
# result.head()




