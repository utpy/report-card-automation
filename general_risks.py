#!/usr/bin/env python
# -*- coding: utf-8 -*-
from data_preprocessing import *

normal_risks = result[['Organization ID', 'IMEI', 'Driver', 'Vehicle', 'Contact', 'Licence', 'datetime', 'speed',
                       'distance', 'Frontal Gap (ft)', 'tailgating_band', 'speed_lane_change_band',
                       'speed_rolling_band', 'speed_pitch_angle_band']]

normal_risks = normal_risks.rename(columns={"tailgating_band": "Aggressive Tailgating"})
normal_risks = normal_risks.rename(columns={"speed_lane_change_band": "Lane Changing"})
normal_risks = normal_risks.rename(columns={"speed_rolling_band": "Risk of Overturning"})
normal_risks = normal_risks.rename(columns={"speed_pitch_angle_band": "Poor Negotiation of Roads/Terrains"})


normal_risks = normal_risks.melt(['Organization ID', 'IMEI', 'Driver', 'Vehicle', 'Contact', 'Licence', 'datetime',
                                  'speed', 'distance', 'Frontal Gap (ft)'],
                                 var_name='Risk Pattern', value_name='Risk Band')


general_risks = pd.DataFrame()
print len(general_risks)

agg_tailgating = normal_risks[normal_risks["Risk Pattern"] == "Aggressive Tailgating"]
agg_tailgating = agg_tailgating[agg_tailgating["Risk Band"] < 2]

lane_change = normal_risks[normal_risks["Risk Pattern"] == "Lane Changing"]
lane_change = lane_change[lane_change["Risk Band"] < 2]

over_turning = normal_risks[normal_risks["Risk Pattern"] == "Risk of Overturning"]
over_turning = over_turning[over_turning["Risk Band"] < 2]

negotiating_roads = normal_risks[normal_risks["Risk Pattern"] == "Poor Negotiation of Roads/Terrains"]
negotiating_roads = negotiating_roads[negotiating_roads["Risk Band"] < 2]

general_risks = general_risks.append([agg_tailgating, lane_change, over_turning, negotiating_roads])
general_risks.sort_values(by=['Driver', 'datetime'], inplace=True)

general_risks = general_risks.drop(["Risk Band"], axis=1)
general_risks.reset_index(inplace=True, drop=True)
general_risks = general_risks[['Organization ID', 'IMEI', 'Driver', 'Vehicle', 'Contact', 'Licence', 'datetime',
                               'speed', 'distance', 'Frontal Gap (ft)', 'Risk Pattern']]

general_risks.to_csv("./general_risks.csv")
print len(general_risks)
print general_risks.head()
