#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Import Libraries

import warnings
# from config_DSSR_server_test_code import *
from config import *
from get_data import *
from functions import *
from data_preprocessing import *
from general_risks import *
from severe_risks import *

code_start_time = dt.utcnow()
print "Code Start Time: ", code_start_time

# ExcelWriter initialisation
dsr = './DSSR.xlsx'
writer = pd.ExcelWriter(dsr, engine='xlsxwriter')
warnings.simplefilter("ignore")

# Set Display option if needed
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.options.display.max_colwidth = 500
# pd.options.display.float_format = '{:.2f}'.format


print imei_list
print start_date
print end_date

print mappings.head()

print "Total packets: ", len(segment_readings)


print general_risks.head()
print "======================================================================================================="
print severe_risks.head()

general_risks.to_csv("./report_card_automation_test_new_braking_collision_risk_general_risks.csv", index=False)
severe_risks.to_csv("./report_card_automation_test_new_braking_collision_risk_severe_risks.csv", index=False)

pdb.set_trace()
