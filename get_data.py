#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pymongo import MongoClient
from config import *


# success_server = MongoClient("mongodb://pritish:new_life@54.68.56.196/safehur_production")
# db_success = success_server["safehur_production"]
# collection_success = db_success["segment_readings"]
#
# segment_reading_query = {"$and": [{"imei": {"$in": imei_list}}, {"speed": {"$gt": 3, "$lt": 110}},
#                                   {"trip_tstamp": {"$gte": start_date, "$lt": end_date}}]}
#
# fields = {"trip_tstamp": 1, "segment_number": 1, "imei": 1, "speed": 1, "latitude": 1, "longitude": 1,
#           "tailgating_band": 1, "speed_braking_band": 1, "tailgating_speed_braking_band": 1, "speed_rolling_band": 1,
#           "speed_pitch_angle_band": 1, "speed_lane_change_band": 1, "speed_orientation_band": 1}
#
# cursor_success = collection_success.find(segment_reading_query, fields)
#
# segment_reading_success_pkts = []
#
# for i in cursor_success:
#     segment_reading_success_pkts.append(i)
#
# segment_readings_success = pd.DataFrame(segment_reading_success_pkts)
# print "Success Packets count: ", len(segment_readings_success)
#
# segment_readings = segment_readings_success


segment_readings = segment_data

segment_readings.sort_values(by=['trip_tstamp'], inplace=True)
print "Total packets: ", len(segment_readings)
