#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Import required Libraries
import pandas as pd
from datetime import timedelta, datetime as dt
import pygsheets

# Parameters to calculate New Braking Bands
config = pd.read_excel("./braking_band_config.xlsx")
config = config.reset_index(drop=True)

# Parameters to calculate New Collision Risk Bands
new_collision_cals = pd.read_excel("./collision_risk_config.xlsx")

# Connect to google sheet using authorized service file
gc = pygsheets.authorize(service_file='./pygsheets-b7619d00ce02.json')

# Use user-vehicle-device-organization mappings spreadsheet from google sheet
sheet = gc.open('uvdo_current_clients')
worksheet = sheet.worksheet_by_title('uvdo')
mappings = worksheet.get_as_df()

# Rename fields
mappings = mappings.rename(columns={"Vehicle Number": "Vehicle"})
mappings = mappings.rename(columns={"Driver Name": "Driver"})
mappings = mappings.rename(columns={"Contact Number": "Contact"})

# Input Single or multiple organizations as per requirement
# 22 - BLR
# 30 - KPT
# 37 - TCI

org_ids = [22, 30, 37]

mappings = mappings[mappings["Organization ID"].isin(org_ids)]

imei_list = []
imei = mappings["IMEI"].astype(str).astype(long)

for i in imei:
    imei_list.append(i)


imei_list = [866600042762443, 866600047209499]
# List of IMEI to be used to fetch data from Mongo DB
print "List of IMEI: ", imei_list


segment_data = pd.read_csv("./test_data_segments.csv")
print len(segment_data)

# Input Start Date and End Date of the period for which the data to be extracted
start_date = "2021-03-06 00:00:00"
end_date = "2021-03-07 00:00:00"

start_date = dt.strptime(start_date, '%Y-%m-%d %H:%M:%S')
end_date = dt.strptime(end_date, '%Y-%m-%d %H:%M:%S')

print "Start Date at IST: ", start_date
print "End Date at IST: ", end_date

# Convert time to UTC as the the data in the Mongo DB is in UTC
start_date = start_date - timedelta(hours=5, minutes=30)
end_date = end_date - timedelta(hours=5, minutes=30)

print "Start Date at UTC (-5:30 Hrs): ", start_date
print "End Date at UTC (-5:30 Hrs): ", end_date
