#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import pandas as pd
# import numpy as np
# import pdb
from data_preprocessing import *


s_risks = result[['Organization ID', 'IMEI', 'Driver', 'Vehicle', 'Contact', 'Licence', 'datetime', 'speed', 'distance',
                  'Frontal Gap (ft)', 'tailgating_band', 'speed_lane_change_band', 'speed_rolling_band',
                  'speed_pitch_angle_band', 'Over-speeding', 'emergency_braking_band', 'collision_risk',
                  'speed_orientation_band']]

s_risks = s_risks.rename(columns={"emergency_braking_band": "Emergency Braking"})
s_risks = s_risks.rename(columns={"collision_risk": "Collision Risk"})
s_risks = s_risks.rename(columns={"speed_orientation_band": "Vehicle Out-of-Control Risk"})
# result['Location'] = ''

s_risks = s_risks.melt(['Organization ID', 'IMEI', 'Driver', 'Vehicle', 'Contact', 'Licence', 'datetime', 'speed',
                        'distance', 'Frontal Gap (ft)'], var_name='Risk Pattern', value_name='Risk Band')


s_risks = s_risks[['Organization ID', 'IMEI', 'Driver', 'Vehicle', 'Contact', 'Licence', 'datetime', 'speed',
                   'distance', 'Frontal Gap (ft)', 'Risk Pattern', 'Risk Band']]

severe_risks = pd.DataFrame()
print len(severe_risks)

collision_risk = s_risks[s_risks["Risk Pattern"] == "Collision Risk"]
collision_risk = collision_risk[collision_risk["Risk Band"] < 3]


emergency_braking_band = s_risks[s_risks["Risk Pattern"] == "Emergency Braking"]
emergency_braking_band = emergency_braking_band[emergency_braking_band["Risk Band"] < 3]


out_of_control = s_risks[s_risks["Risk Pattern"] == "Vehicle Out-of-Control Risk"]
out_of_control = out_of_control[out_of_control["Risk Band"] < 3]


over_speeding = s_risks[s_risks["Risk Pattern"] == "Over-speeding"]
over_speeding = over_speeding[over_speeding["Risk Band"] == 1]

severe_risks = severe_risks.append([collision_risk, emergency_braking_band, out_of_control, over_speeding])
severe_risks.sort_values(by=['Driver', 'datetime'], inplace=True)

severe_risks = severe_risks.drop(["Risk Band"], axis=1)
severe_risks.reset_index(inplace=True, drop=True)
severe_risks = severe_risks[['Organization ID', 'IMEI', 'Driver', 'Vehicle', 'Contact', 'Licence', 'datetime', 'speed',
                             'distance', 'Frontal Gap (ft)', 'Risk Pattern']]
# print len(risks)
# print "Google Earth File Saved in ../Amazon/DOT/results"
print len(severe_risks)
print severe_risks.head()
