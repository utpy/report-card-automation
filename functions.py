#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from get_data import *


def one_hour_time_band(tme):
    global time_band_one_hour
    if "00:00:00" <= tme < "01:00:00":
        time_band_one_hour = "00:00-01:00"
    elif "01:00:00" <= tme < "02:00:00":
        time_band_one_hour = "01:00-02:00"
    elif "02:00:00" <= tme < "03:00:00":
        time_band_one_hour = "02:00-03:00"
    elif "03:00:00" <= tme < "04:00:00":
        time_band_one_hour = "03:00-04:00"
    elif "04:00:00" <= tme < "05:00:00":
        time_band_one_hour = "04:00-05:00"
    elif "05:00:00" <= tme < "06:00:00":
        time_band_one_hour = "05:00-06:00"
    elif "06:00:00" <= tme < "07:00:00":
        time_band_one_hour = "06:00-07:00"
    elif "07:00:00" <= tme < "08:00:00":
        time_band_one_hour = "07:00-08:00"
    elif "08:00:00" <= tme < "09:00:00":
        time_band_one_hour = "08:00-09:00"
    elif "09:00:00" <= tme < "10:00:00":
        time_band_one_hour = "09:00-10:00"
    elif "10:00:00" <= tme < "11:00:00":
        time_band_one_hour = "10:00-11:00"
    elif "11:00:00" <= tme < "12:00:00":
        time_band_one_hour = "11:00-12:00"
    elif "12:00:00" <= tme < "13:00:00":
        time_band_one_hour = "12:00-13:00"
    elif "13:00:00" <= tme < "14:00:00":
        time_band_one_hour = "13:00-14:00"
    elif "14:00:00" <= tme < "15:00:00":
        time_band_one_hour = "14:00-15:00"
    elif "15:00:00" <= tme < "16:00:00":
        time_band_one_hour = "15:00-16:00"
    elif "16:00:00" <= tme < "17:00:00":
        time_band_one_hour = "16:00-17:00"
    elif "17:00:00" <= tme < "18:00:00":
        time_band_one_hour = "17:00-18:00"
    elif "18:00:00" <= tme < "19:00:00":
        time_band_one_hour = "18:00-19:00"
    elif "19:00:00" <= tme < "20:00:00":
        time_band_one_hour = "19:00-20:00"
    elif "20:00:00" <= tme < "21:00:00":
        time_band_one_hour = "20:00-21:00"
    elif "21:00:00" <= tme < "22:00:00":
        time_band_one_hour = "21:00-22:00"
    elif "22:00:00" <= tme < "23:00:00":
        time_band_one_hour = "22:00-23:00"
    elif "23:00:00" <= tme < "00:00:00":
        time_band_one_hour = "23:00-00:00"
    return time_band_one_hour


def three_hour_time_band(tme):
    global time_band_three_hour
    if "00:00:00" <= tme < "03:00:00":
        time_band_three_hour = "00:00-03:00"
    elif "03:00:00" <= tme < "06:00:00":
        time_band_three_hour = "03:00-06:00"
    elif "06:00:00" <= tme < "09:00:00":
        time_band_three_hour = "06:00-09:00"
    elif "09:00:00" <= tme < "12:00:00":
        time_band_three_hour = "09:00-12:00"
    elif "12:00:00" <= tme < "15:00:00":
        time_band_three_hour = "12:00-15:00"
    elif "15:00:00" <= tme < "18:00:00":
        time_band_three_hour = "15:00-18:00"
    elif "18:00:00" <= tme < "21:00:00":
        time_band_three_hour = "18:00-21:00"
    elif "21:00:00" <= tme < "00:00:00":
        time_band_three_hour = "21:00-00:00"
    return time_band_three_hour


def speed_band_interval_five(spd):
    global spd_bnd
    if 0 < spd <= 5:
        spd_bnd = "0-5"
    elif 5 < spd <= 10:
        spd_bnd = "5-10"
    elif 10 < spd <= 15:
        spd_bnd = "10-15"
    elif 15 < spd <= 20:
        spd_bnd = "15-20"
    elif 20 < spd <= 25:
        spd_bnd = "20-25"
    elif 25 < spd <= 30:
        spd_bnd = "25-30"
    elif 30 < spd <= 35:
        spd_bnd = "30-35"
    elif 35 < spd <= 40:
        spd_bnd = "35-40"
    elif 40 < spd <= 45:
        spd_bnd = "40-45"
    elif 45 < spd <= 50:
        spd_bnd = "45-50"
    elif 50 < spd <= 55:
        spd_bnd = "50-55"
    elif 55 < spd <= 60:
        spd_bnd = "55-60"
    elif 60 < spd <= 65:
        spd_bnd = "60-65"
    elif 65 < spd <= 70:
        spd_bnd = "65-70"
    elif 70 < spd <= 75:
        spd_bnd = "70-75"
    elif 75 < spd <= 80:
        spd_bnd = "75-80"
    elif 80 < spd <= 85:
        spd_bnd = "80-85"
    elif 85 < spd <= 90:
        spd_bnd = "85-90"
    elif 90 < spd <= 95:
        spd_bnd = "90-95"
    elif 95 < spd <= 100:
        spd_bnd = "95-100"
    elif 100 < spd <= 105:
        spd_bnd = "100-105"
    elif 105 < spd <= 110:
        spd_bnd = "105-110"
    elif 110 < spd <= 115:
        spd_bnd = "110-115"
    elif 115 < spd <= 120:
        spd_bnd = "115-120"
    elif 120 < spd <= 125:
        spd_bnd = "120-125"
    elif 125 < spd <= 130:
        spd_bnd = "125-130"
    elif 130 < spd <= 135:
        spd_bnd = "130-135"
    elif 135 < spd <= 140:
        spd_bnd = "135-140"
    elif 140 < spd <= 145:
        spd_bnd = "140-145"
    elif 145 < spd <= 150:
        spd_bnd = "145-150"
    return str(spd_bnd)


def speed_band_interval_ten(spd):
    global spd_bnd
    if 0 < spd <= 10:
        spd_bnd = "0-10"
    elif 10 < spd <= 20:
        spd_bnd = "10-20"
    elif 20 < spd <= 30:
        spd_bnd = "20-30"
    elif 30 < spd <= 40:
        spd_bnd = "30-40"
    elif 40 < spd <= 50:
        spd_bnd = "40-50"
    elif 50 < spd <= 60:
        spd_bnd = "50-60"
    elif 60 < spd <= 70:
        spd_bnd = "60-70"
    elif 70 < spd <= 80:
        spd_bnd = "70-80"
    elif 80 < spd <= 90:
        spd_bnd = "80-90"
    elif 90 < spd <= 100:
        spd_bnd = "90-100"
    elif 100 < spd <= 110:
        spd_bnd = "100-110"
    elif 110 < spd <= 120:
        spd_bnd = "110-120"
    elif 120 < spd <= 130:
        spd_bnd = "120-130"
    elif 130 < spd <= 140:
        spd_bnd = "130-140"
    elif 140 < spd <= 150:
        spd_bnd = "140-150"
    return str(spd_bnd)


def braking(delta_time, delta_speed):
    if delta_speed < 0:
        br = (delta_speed / delta_time) * 5 / 18
    else:
        br = 0
    return br


def add_band(x):
    band_data = config.apply(lambda y: y['band'] if (y['min_dec'] <= x['dec'] < y['max_dec']
                                                     and x['Speed Band Interval Five'] == y['velocity_band']
                                                     and x['Vehicle Type'] == y['Vehicle Type']) else None, axis=1)

    band_data = band_data.dropna(axis=0, how='all')
    if band_data.empty:
        band_data = np.nan
    else:
        band_data = band_data.to_string(index=False)
    return band_data


def over_speed(spd):
    if spd > 80:
        return 1
    else:
        return 0


def tail_dist(spd):
    dist = (spd*5/18) * 0.45 * 3.28084
    return dist
